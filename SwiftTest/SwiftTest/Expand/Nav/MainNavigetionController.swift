//
//  MainNavigetionController.swift
//  SwiftTest
//
//  Created by Shen on 2016/11/29.
//  Copyright © 2016年 Joinwe. All rights reserved.
//

import UIKit

class MainNavigetionController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    //第一次使用这个类的时候调用一次（1个类只会调用1次）
    override class func initialize() {
        //    1.设置导航栏主题
        //self.automaticallyAdjustsScrollViewInsets = false;
        //    取出appearance对象
        let navBar = UINavigationBar.appearance();
        //    2.设置背景
        navBar.shadowImage = UIImage() //去除导航栏下面的灰线
        navBar.isTranslucent = false
        //navBar.clipsToBounds = true
        
        navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navBar.barTintColor = kAppMainColor
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.default
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        if viewControllers.count > 0 {
            viewController.hidesBottomBarWhenPushed = true
        }
        super.pushViewController(viewController, animated: animated)
    }

}
