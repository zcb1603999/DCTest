//
//  ApiRequest.swift
//  SwiftTest
//
//  Created by Joinwe on 16/9/13.
//  Copyright © 2016年 Joinwe. All rights reserved.
//

import UIKit
import AFNetworking

/** 响应结构体*/
struct ApiResponse {
    /** 请求结果代码*/
    var code:Int?;
    /** 结果数据*/
    var data:NSDictionary?;
    /** 消息*/
    var message:String?;
    
}


class ApiRequest: NSObject {
    var service:ApiService?;
    var mgr:AFHTTPSessionManager?;
    
    
    var dicParams: NSMutableDictionary{
        let dicParams = NSMutableDictionary();
        return dicParams;
    }
    
    
    func setParams(_ param: NSObject, key: String){
        self.dicParams.setObject(param, forKey: key as NSCopying);
    }
    
    func getParams(_ key: String) -> AnyObject {
        return self.dicParams.object(forKey: key)! as AnyObject;
    }
    
    func removeAllParams(){
        self.dicParams.removeAllObjects();
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init(servletInterface:ApiService){
        super.init();
        self.service = servletInterface;
    }
    
    
    func callWithCallBack(_ finsished:(_ result:AnyObject?) ->()){
        let url = self.service?.url;
        let parameter = self.dicParams;
        
        print("开始请求：\(url)");
        print("参数：\(parameter)");
        
        
        mgr = AFHTTPSessionManager();
        
        if self.service?.method == ApiRequestMethod.apiRequestMethodGET {
            
        }else if self.service?.method == ApiRequestMethod.apiRequestMethodPOST {
            
        }else if self.service?.method == ApiRequestMethod.apiRequestMethodDELETE {
            
        }else if self.service?.method == ApiRequestMethod.apiRequestMethodUPDATE {
            
        }
    }

    
}
