//
//  ApiService.swift
//  SwiftTest
//
//  Created by Joinwe on 16/9/13.
//  Copyright © 2016年 Joinwe. All rights reserved.
//

import Foundation

enum ApiRequestMethod: NSInteger {
    case apiRequestMethodGET    = 0;
    case apiRequestMethodPOST   = 1;
    case apiRequestMethodDELETE = 2;
    case apiRequestMethodUPDATE = 3;
}

class ApiService: NSObject {
    var url:String?;
    var method:ApiRequestMethod?;
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init(url:String, method:ApiRequestMethod?){
        super.init();
        self.url = url;
        self.method = method;
    }
}
