//
//  SingleRequest.swift
//  SwiftTest
//
//  Created by Joinwe on 16/9/18.
//  Copyright © 2016年 Joinwe. All rights reserved.
//

import Foundation
import AFNetworking

/** 响应结构体*/
struct SingleResponse {
    /** 请求结果代码*/
    var code:Int?;
    /** 结果数据*/
    var data:NSDictionary?;
    /** 消息*/
    var message:String?;
    
}

//单例写法第三种
private let sharedSingleRequest = SingleRequest()


class SingleRequest{
    var service:ApiService?;
    var mgr:AFHTTPSessionManager?;
    
    //必须保证init方法的私有性，只有这样，才能保证单例是真正唯一的，避免外部对象通过访问init方法创建单例类的其他实例。由于Swift中的所有对象都是由公共的初始化方法创建的，我们需要重写自己的init方法，并设置其为私有的。
//    private init() {
//        super.init();
//    };
    
    
    var dicParams: NSMutableDictionary{
        let dicParams = NSMutableDictionary();
        return dicParams;
    }
    
    func setParams(_ param: NSObject, key: String){
        self.dicParams.setObject(param, forKey: key as NSCopying);
    }
    
    func getParams(_ key: String) -> AnyObject {
        return self.dicParams.object(forKey: key)! as AnyObject;
    }
    
    func removeParamForKey(key: String){
        self.dicParams.removeObject(forKey: key);
    }
    
    func removeAllParams(){
        self.dicParams.removeAllObjects();
    }
    
    //单例写法第一种，已经不能用喽
/**
    class var sharedInstance1: SingleRequest {
        struct Static {
            //就因为这里不能用的
            static var onceToken: dispatch_once_t = 0;
            static var instance: SingleRequest? = nil;
        }

        dispatch_once(&Static.onceToken) {
            Static.instance = SingleRequest();
        }
     
        return Static.instance!;
    }
*/
 
    //单例写法第二种，第一种的升级版
    class var sharedInstance2: SingleRequest {
        struct Static {
            static let instance = SingleRequest();
        }
        return Static.instance;
    }
    
    
    //单例写法第三种
    class var sharedInstance3: SingleRequest {
        return sharedSingleRequest;
    }
    
    
    //单例写法终极版
    static let sharedInstance = SingleRequest();
    
    
    func callWithParameters(baseUrl:String, path: String, requestType: ApiRequestMethod, parameters: NSMutableDictionary, closure:(_ resultDic: NSMutableDictionary) -> Void){
            let resultDic = NSMutableDictionary();
        
            closure(resultDic);
    }

}
