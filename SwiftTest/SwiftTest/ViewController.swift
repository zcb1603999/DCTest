//
//  ViewController.swift
//  SwiftTest
//
//  Created by Joinwe on 16/8/17.
//  Copyright © 2016年 Joinwe. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        test1();
        
    }

    
    
    func test1() {
        sayHello("大神")
        
        if let bounds = minMax([8, -6, 2, 109, 3, 71]){
                print("最大值为 \(bounds.max) 最小值为 \(bounds.min)")
        }
    }
    
    
    //创建方法
    func sayHello(_ name:String){
        print(name + "，你好！")
    }
    
    //打印方法
    func var_dump(_ name:String, parmars:AnyObject){
        print("\(name)打印的结果是 \(parmars)")
    }
    
    
    //得到数组中最小最大的值
    func minMax(_ array:[Int]) -> (min:Int, max:Int)? {
        if array.isEmpty{return nil}
        var currentMin = array[0]
        var currentMax = array[0]
        for value in array[1..<array.count] {
            if value < currentMin {
                currentMin = value
            }else if value > currentMax{
                currentMax = value
            }
        }
        
        return (currentMin, currentMax);
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

