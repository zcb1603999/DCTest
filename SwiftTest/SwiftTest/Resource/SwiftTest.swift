//
//  SwiftTest.swift
//  SwiftTest
//
//  Created by Shen on 2016/11/30.
//  Copyright © 2016年 Joinwe. All rights reserved.
//

import UIKit

let kDEVICE_WIDTH = UIScreen.main.bounds.size.width
let kDEVICE_HEIGHT = UIScreen.main.bounds.size.height

func RGBA (r: CGFloat, g: CGFloat, b: CGFloat, a: CGFloat) -> UIColor {
    return UIColor(red: r/255.0, green: g/255.0, blue: b/255.0, alpha: a)
}


let kAppMainColor = RGBA(r: 10, g: 162, b: 132, a: 1)
