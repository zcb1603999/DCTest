//
//  AppDelegate.swift
//  SwiftTest
//
//  Created by Joinwe on 16/8/17.
//  Copyright © 2016年 Joinwe. All rights reserved.
//

import UIKit
import FDFullscreenPopGesture
import MMDrawerController

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        
        
        self.window = UIWindow(frame: UIScreen.main.bounds);
        self.window?.backgroundColor = UIColor.white;
        self.window?.makeKey();
        
       
        let nav1 = MainNavigetionController(rootViewController:IndexViewController(backgroundColor:UIColor.lightGray));
        nav1.navigationItem.title = "圈子";
        nav1.fd_fullscreenPopGestureRecognizer.isEnabled = true
        nav1.tabBarItem = UITabBarItem(title: "圈子",image: UIImage(named: "quanzi_no"), selectedImage: UIImage(named: "quanzi_yes"))
        
        
        
        let nav2 = MainNavigetionController(rootViewController:IndexViewController(backgroundColor:UIColor.green));
        nav2.navigationItem.title = "附近";
        nav2.fd_fullscreenPopGestureRecognizer.isEnabled = true
        nav2.tabBarItem = UITabBarItem(title: "附近",image: UIImage(named: "fujin_no"), selectedImage: UIImage(named: "fujin_yes"))
        
        
        let nav3 = MainNavigetionController(rootViewController:IndexViewController(backgroundColor:UIColor.blue));
        nav3.navigationItem.title = "我";
        nav3.fd_fullscreenPopGestureRecognizer.isEnabled = true
        nav3.tabBarItem = UITabBarItem(title: "我",image: UIImage(named: "wo_no"), selectedImage: UIImage(named: "wo_yes"))
        
        
        let tabBar = UITabBarController();
        tabBar.viewControllers = [nav1, nav2, nav3];
        
        
        let mainController = MMDrawerController()
        mainController.centerViewController = tabBar
        
        mainController.leftDrawerViewController = CeshiViewController()
        
        self.window?.rootViewController = mainController;
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

