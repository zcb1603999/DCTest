//
//  IndexViewController.swift
//  SwiftTest
//
//  Created by Joinwe on 16/9/12.
//  Copyright © 2016年 Joinwe. All rights reserved.
//

import UIKit
import MMDrawerController

class IndexViewController: UIViewController {
    
    var backViewColor: UIColor!;
    
    init(backgroundColor:UIColor){
        super.init(nibName: nil, bundle: nil);
        self.backViewColor = backgroundColor;
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = self.backViewColor
        self.automaticallyAdjustsScrollViewInsets = false
        self.extendedLayoutIncludesOpaqueBars = false
        self.edgesForExtendedLayout = UIRectEdge.bottom
        // Do any additional setup after loading the view.
        
        
        let imageView = UIImageView(frame:CGRect(x: 0, y: 20, width: 320, height: 100));
        imageView.image = UIImage(named: "");
        self.view.addSubview(imageView);
        
        
        self.navigationItem.title = "测试";
        
        
        /// 获取资源路径
        let bundle = Bundle.main.bundleIdentifier;
        print("资源bundleId \(bundle)");

        
        
        let button = UIButton(type: UIButtonType.custom)
        button.frame = CGRect(x:10, y:64, width:(kDEVICE_WIDTH - 20), height:100)
        button.backgroundColor = UIColor.brown
        button.titleLabel?.text = "测试"
        button.addTarget(self, action: #selector(btnClick), for: UIControlEvents.touchUpInside)
        self.view.addSubview(button)
        
        
        
        
        let leftBarButton = UIButton(type: UIButtonType.custom)
        leftBarButton.frame = CGRect(x:0,y:0,width:50,height:44)
        leftBarButton.setImage(UIImage(named : "wo_yes"), for: UIControlState.normal)
        //leftBarButton.backgroundColor = UIColor.green
        leftBarButton.adjustsImageWhenHighlighted = false
        leftBarButton.setTitleColor(UIColor.black, for: UIControlState.normal)
        leftBarButton.addTarget(self, action: #selector(showCeshi), for: UIControlEvents.touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: leftBarButton)
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.mm_drawerController.showsShadow = true
        self.mm_drawerController.maximumLeftDrawerWidth = 240.0
        self.mm_drawerController.openDrawerGestureModeMask = MMOpenDrawerGestureMode.all
        self.mm_drawerController.closeDrawerGestureModeMask = MMCloseDrawerGestureMode.all
    }
    
    
    func btnClick() -> Void {
        self.navigationController?.pushViewController(CeshiViewController(), animated: true)
    }

    func showCeshi() -> Void {
        self.mm_drawerController.toggle(MMDrawerSide.left, animated: true, completion: nil)
    }
    
    /**几种输出方法*/
    func show() {
        print("这是一个测试");
        debugPrint("这是第二个测试");
        CFShow("这是第三个测试" as CFTypeRef!);
        
        let position = CGPoint(x : 10.0, y : 20);
        dump(position);
        
        
        let str = "Xcode";
        print("Hello " + str);
        print("Hello \(str)");
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
