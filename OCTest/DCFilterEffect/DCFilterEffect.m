//
//  DCFilterEffect.m
//  DCRequestTest
//
//  Created by Joinwe on 16/6/7.
//  Copyright © 2016年 Joinwe. All rights reserved.
//

#import "DCFilterEffect.h"

@interface DCFilterEffect()

@end

@implementation DCFilterEffect

- (instancetype)initWithImage:(UIImage *)image filterName:(NSString *)name{
    self = [super init];
    if (self){
        // 将UIImage转换成CIImage
        CIImage *ciImage = [[CIImage alloc] initWithImage:image];
        
        // 创建滤镜
        CIFilter *filter = [CIFilter filterWithName:name
                                      keysAndValues:kCIInputImageKey, ciImage, nil];
        [filter setDefaults];
        
        // 获取绘制上下文
        CIContext *context = [CIContext contextWithOptions:nil];
        
        // 渲染并输出CIImage
        CIImage *outputImage = [filter outputImage];
        
        // 创建CGImage句柄
        CGImageRef cgImage = [context createCGImage:outputImage
                                           fromRect:[outputImage extent]];
        
        _result = [UIImage imageWithCGImage:cgImage];
        
        // 释放CGImage句柄
        CGImageRelease(cgImage);
    }
    return self;
}


@end
