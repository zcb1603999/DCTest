//
//  DCFilterEffect.h
//  DCRequestTest
//
//  Created by Joinwe on 16/6/7.
//  Copyright © 2016年 Joinwe. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface DCFilterEffect : NSObject

@property (nonatomic, strong) UIImage *result;

- (instancetype)initWithImage:(UIImage *)image filterName:(NSString *)name;

@end
