//
//  ThreeTimesArray.m
//  DCRequestTest
//
//  Created by Joinwe on 16/5/30.
//  Copyright © 2016年 Joinwe. All rights reserved.
//

#import "ThreeTimesArray.h"

@implementation ThreeTimesArray

- (void)insertObject:(id)object inNumbersAtIndex:(NSUInteger)index {
    [self.numbers insertObject:object atIndex:index];
    NSLog(@"insert %@n", object);
}
- (void)removeObjectFromNumbersAtIndex:(NSUInteger)index {
    [self.numbers removeObjectAtIndex:index];
    NSLog(@"remove");
}


@end
