//
//  main.m
//  DCTest
//
//  Created by Joinwe on 16/6/7.
//  Copyright © 2016年 Joinwe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

//#import "People.h"


int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
    
//    People *person = [[People alloc] init];
//    [person setGivenName:@"Bob"];
//    [person setSurnName:@"Jones"];
//    
//    NSLog(@"%@ %@", [person givenName], [person surnName]);
//    
//    return 0;
}
