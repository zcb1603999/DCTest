//
//  TableViewController.m
//  DCTest
//
//  Created by Joinwe on 16/7/13.
//  Copyright © 2016年 Joinwe. All rights reserved.
//

#import "TableViewController.h"

@interface TableViewController ()<UITableViewDelegate,UITableViewDataSource>

@property NSMutableArray *groupArr;

@property NSMutableArray *selectedArr;

@property UITableView *tableView;

@property NSMutableDictionary *dic;

@end

@implementation TableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 320, 568) style:UITableViewStylePlain];
    
    _tableView.delegate = self;
    
    _tableView.dataSource = self;
    
//    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    _tableView.sectionHeaderHeight = 40;
    
    [self.view addSubview:_tableView];
    
    
    NSMutableDictionary *dic1 = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"范旭东", @"name", @"c_0", @"imageName", @"[在线]他开着你的花，在每一个晚霞，靠着你的肩膀绣着枝桠", @"lastMess", @"2G", @"net", @"mainCell", @"cell", @"NO", @"state", nil];
    
    NSMutableDictionary *dic2 = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"谢遵义", @"name", @"c_1", @"imageName", @"[在线]他开着你的花，在每一个晚霞，靠着你的肩膀绣着枝桠", @"lastMess", @"2G", @"net", @"mainCell", @"cell", @"NO", @"state", nil];
    
    NSMutableDictionary *dic3 = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"尚俊鹏", @"name", @"c_2", @"imageName", @"[在线]他开着你的花，在每一个晚霞，靠着你的肩膀绣着枝桠", @"lastMess", @"2G", @"net", @"mainCell", @"cell", @"NO", @"state", nil];
    
    NSMutableDictionary *dic4 = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"李长春", @"name", @"c_3", @"imageName", @"[在线]他开着你的花，在每一个晚霞，靠着你的肩膀绣着枝桠", @"lastMess", @"2G", @"net", @"mainCell", @"cell", @"NO", @"state", nil];
    
    NSMutableDictionary *dic5 = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"牛诗雨", @"name", @"c_4", @"imageName", @"[在线]他开着你的花，在每一个晚霞，靠着你的肩膀绣着枝桠", @"lastMess", @"2G", @"net", @"mainCell", @"cell", @"NO", @"state", nil];
    
    NSMutableArray *groups0 = [[NSMutableArray alloc] initWithObjects:dic1, dic2, dic4, dic5, nil];
    
    NSMutableArray *groups1 = [[NSMutableArray alloc] initWithObjects:dic1, dic2, dic3, dic5, nil];
    
    NSMutableArray *groups2 = [[NSMutableArray alloc] initWithObjects:dic1, dic2, dic3, dic4, dic5, nil];
    
    NSMutableArray *groups3 = [[NSMutableArray alloc] initWithObjects:dic2, dic3, dic4, dic5, nil];
    
    NSMutableArray *groups4 = [[NSMutableArray alloc] initWithObjects:dic1, dic2, dic3, dic4, dic5, nil];
    
    NSMutableArray *groups5 = [[NSMutableArray alloc] initWithObjects:dic1, dic2, dic3, dic4, nil];
    
    _dic = [[NSMutableDictionary alloc] initWithCapacity:0];
    
    [_dic setObject:groups0 forKey:@"0"];
    
    [_dic setObject:groups1 forKey:@"1"];
    
    [_dic setObject:groups2 forKey:@"2"];
    
    [_dic setObject:groups3 forKey:@"3"];
    
    [_dic setObject:groups4 forKey:@"4"];
    
    [_dic setObject:groups5 forKey:@"5"];
    
    _groupArr = [[NSMutableArray alloc] initWithObjects:@"丶少一些盼望", @"丶少一些憧憬", @"丶多些珍惜眼下的", @"丶习惯这样的节奏", @"丶并逐渐", @"丶喜欢这样的方式", nil];
    
    _selectedArr = [[NSMutableArray alloc] initWithCapacity:0];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return _groupArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *sectionString = [NSString stringWithFormat:@"%ld", (long)indexPath.section];
    
    if ([_dic[sectionString][indexPath.row][@"cell"] isEqualToString:@"mainCell"]) {
        return 60;
    }else{
        return 40;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if ([_selectedArr containsObject:_groupArr[section]]) {
        NSString *sectionString = [NSString stringWithFormat:@"%ld", (long)section];
        NSMutableArray *arr = _dic[sectionString];
        return arr.count;
    }else {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *sectionString = [NSString stringWithFormat:@"%ld", (long)indexPath.section];
    if ([_dic[sectionString][indexPath.row][@"cell"] isEqualToString:@"mainCell"]) {
        NSString *identifier = @"mainCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"mainCell"];
        }
        cell.backgroundColor = [UIColor blueColor];
        return cell;
    }else if ([_dic[sectionString][indexPath.row][@"cell"] isEqualToString:@"attchedCell"]) {
        NSString *identifier = @"attchedCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"attchedCell"];
        }
        return cell;
    }
    return nil;
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 40)];
    
    view.backgroundColor = [UIColor greenColor];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    btn.frame = CGRectMake(0, 0, 320, 39);
    
    btn.backgroundColor = [UIColor grayColor];
    
    btn.tag = 100 + section;
    
    [btn addTarget:self action:@selector(sectionBtn:) forControlEvents:UIControlEventTouchUpInside];
    
    [view addSubview:btn];
    
    return view;
}

- (void)sectionBtn:(UIButton *)sender{
    
    NSString *sectionName = _groupArr[sender.tag-100];
    
    if ([_selectedArr containsObject:sectionName]) {
        
        [_selectedArr removeObject:sectionName];
    }else {
        
        [_selectedArr addObject:sectionName];
    }
    
    [_tableView reloadData];
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSString *sectionString = [NSString stringWithFormat:@"%ld", indexPath.section];
    
    NSIndexPath *path = [NSIndexPath indexPathForItem:indexPath.row+1 inSection:indexPath.section];
    
    if ([_dic[sectionString][indexPath.row][@"cell"] isEqualToString:@"mainCell"]) {
        
        if ([_dic[sectionString][indexPath.row][@"state"] boolValue]) {
            
            _dic[sectionString][indexPath.row][@"state"] = @"NO";
            
            [_dic[sectionString] removeObjectAtIndex:path.row];
            
            [tableView beginUpdates];
            
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:path] withRowAnimation:UITableViewRowAnimationMiddle];
            
            [tableView endUpdates];
        }else {
            _dic[sectionString][indexPath.row][@"state"] = @"YES";
            
            NSMutableDictionary *other = [NSMutableDictionary dictionary];
            
            
            [other setObject:@"attchedCell" forKey:@"cell"];
            
            [_dic[sectionString] insertObject:other atIndex:path.row];
            
            NSLog(@"%@",_dic);
            
            [tableView beginUpdates];
            
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:path] withRowAnimation:UITableViewRowAnimationMiddle];
            
            [tableView endUpdates];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}


@end
