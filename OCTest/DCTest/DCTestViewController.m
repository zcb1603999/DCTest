//
//  DCTestViewController.m
//  DCTest
//
//  Created by Joinwe on 16/6/24.
//  Copyright © 2016年 Joinwe. All rights reserved.
//

#import "DCTestViewController.h"
#import <Masonry/Masonry.h>

#define kWeakSelf(weakSelf)  __weak __typeof(&*self)weakSelf = self
#define kIOSVersion ((float)[[[UIDevice currentDevice] systemVersion] doubleValue])

#define kDeviceSize [UIScreen mainScreen].bounds.size

@interface DCTestViewController ()

@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *passLabel;
@property (nonatomic, strong) UITextField *nameTextFile;
@property (nonatomic, strong) UITextField *passTextFile;
@property (nonatomic, strong) UIButton *submit;

@end

@implementation DCTestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor lightGrayColor];
    
    if (kIOSVersion >= 7.0) {
        self.automaticallyAdjustsScrollViewInsets = NO;
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    
    [self createView];
    [self setFrame];
    
}

- (void)createView{
    self.nameLabel = [UILabel new];
    self.nameLabel.text = @"用户名";
    [self.view addSubview:self.nameLabel];
    
    self.nameTextFile = [UITextField new];
    self.nameTextFile.placeholder = @"请输入用户名";
    [self.view addSubview:self.nameTextFile];
    
    self.passLabel = [UILabel new];
    self.passLabel.text = @"密   码";
    [self.view addSubview:self.passLabel];
    
    self.passTextFile = [UITextField new];
    self.passTextFile.placeholder = @"请输入密码";
    [self.view addSubview:self.passTextFile];
    
    self.submit = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.submit setTitle:@"提交" forState:UIControlStateNormal];
    self.submit.backgroundColor = [UIColor greenColor];
    [self.submit addTarget:self action:@selector(btnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.submit];
    
    
}

- (void)btnClick{
    NSLog(@"登录按钮");
}

- (void)setFrame{
    kWeakSelf(weakSelf);
    
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.mas_equalTo(40);
        make.size.mas_equalTo(CGSizeMake(60, 20));
    }];
    
    [self.nameTextFile mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(weakSelf.nameLabel);
        make.left.equalTo(weakSelf.nameLabel.mas_right).with.offset(10);
        make.height.equalTo(weakSelf.nameLabel);
        make.width.equalTo(@150);
    }];
    
    [self.passLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(weakSelf.nameLabel.mas_bottom).with.offset(20);
        make.left.equalTo(weakSelf.nameLabel);
        make.size.equalTo(weakSelf.nameLabel);
    }];
    
    [self.passTextFile mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.nameTextFile);
        make.top.equalTo(weakSelf.passLabel);
        make.size.equalTo(weakSelf.nameTextFile);
    }];
    
    [self.submit mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@20);
        make.top.equalTo(weakSelf.passLabel).with.offset(50);
        make.size.mas_equalTo(CGSizeMake(280, 50));
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
