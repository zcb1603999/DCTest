//
//  DCADView.h
//  DCTest
//
//  Created by Joinwe on 16/7/7.
//  Copyright © 2016年 Joinwe. All rights reserved.
//


typedef enum {
    FullScreenDCADType      = 1,  //全屏广告
    LogoDCADType            = 0,  //带logo的广告
} DCADType;


#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>

/**屏幕的宽高*/
#define     kDeviceHeight      [[UIScreen mainScreen] bounds].size.height
#define     kDeviceWidth       [[UIScreen mainScreen] bounds].size.width


typedef void(^DCADBlock)(NSInteger tag);

@interface DCADView : UIView

/**广告视图*/
@property (nonatomic, strong) UIImageView *DCADImageView;
/**窗口*/
@property (nonatomic, strong) UIWindow *window;
/**倒计时*/
@property (nonatomic, assign) NSInteger DCADTime;
/**跳过*/
@property (nonatomic, strong) UIButton *skipButton;
@property (nonatomic, copy)   DCADBlock clickBlock;

/**
 *  初始化方法
 *
 *  @param window   窗口，一般是主窗口
 *  @param type     广告的类型
 *  @param imageUrl 图片的链接
 *
 *  @return 返回一个类对象
 */
- (instancetype)initWithWindow:(UIWindow *)window andType:(DCADType)type andImageUrl:(NSString *)imageUrl;

@end
