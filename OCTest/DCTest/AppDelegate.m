//
//  AppDelegate.m
//  DCTest
//
//  Created by Joinwe on 16/6/7.
//  Copyright © 2016年 Joinwe. All rights reserved.
//

#import "AppDelegate.h"
#import "DCTabBarController.h"
#import "ViewController.h"
#import "DCWaterViewController.h"
#import "DCViewController.h"
#import "DCTestViewController.h"
#import "FMDBTestViewController.h"
#import "RuntimeViewController.h"

#import "ADView/DCADView.h"

#import "TableViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window.backgroundColor = [UIColor whiteColor];
    ViewController *view = [[ViewController alloc] init];
    self.window.rootViewController = view;
    [self.window makeKeyAndVisible];
    
    NSString *path = [NSHomeDirectory() stringByAppendingString:@"Documents"];
    NSLog(@"%@",path);

    
    [self notification];
    
    //接收通知参数
    UILocalNotification *notification=[launchOptions valueForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    NSDictionary *userInfo= notification.userInfo;
    
    
    return YES;
}

//本地通知
- (void)notification{
    //如果已经得到授权，就直接添加本地通知，否则申请询问授权
    if ([[UIApplication sharedApplication] currentUserNotificationSettings].types != UIUserNotificationTypeNone) {
        [self addLocalNotification];
    }else{
        [[UIApplication sharedApplication]registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|UIUserNotificationTypeSound  categories:nil]];
    }
}


- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings{
    if (notificationSettings.types!=UIUserNotificationTypeNone) {
        [self addLocalNotification];
    }
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification{
    NSLog(@"测试一下");
}

- (void)addLocalNotification{
    //定义本地通知对象
    UILocalNotification *notification=[[UILocalNotification alloc]init];
    //设置调用时间
    notification.fireDate=[NSDate dateWithTimeIntervalSinceNow:5];//立即触发
    //设置通知属性
    notification.alertBody=@"HELLO，我是本地通知哦!"; //通知主体
    notification.applicationIconBadgeNumber=1;//应用程序图标右上角显示的消息数
    notification.alertAction=@"打开应用"; //待机界面的滑动动作提示
    notification.soundName=UILocalNotificationDefaultSoundName;//收到通知时播放的声音，默认消息声音
    //调用通知
    [[UIApplication sharedApplication] scheduleLocalNotification:notification];
}

- (void)startView{
    //    启动广告测试
    //    DCADView *adView = [[DCADView alloc] initWithWindow:self.window andType:LogoDCADType andImageUrl:@"http://blog.duicode.com/wp-content/uploads/2016/07/meinv.jpg"];
    //    adView.clickBlock = ^(NSInteger tag){
    //        switch (tag) {
    //            case 1100:{
    //                    DCTabBarController *tabBar = [[DCTabBarController alloc] init];
    //
    //
    //
    //                    ViewController *view = [[ViewController alloc] init];
    //                    self.window.rootViewController = view;
    
    //                    DCWaterViewController *view = [[DCWaterViewController alloc] init];
    //                    self.window.rootViewController = view;
    //
    //                    DCViewController *view = [[DCViewController alloc] init];
    //                    self.window.rootViewController = view;
    
    //                    DCTestViewController *masonryView = [[DCTestViewController alloc] init];
    //                    self.window.rootViewController = masonryView;
    //
    //                    FMDBTestViewController *View = [[FMDBTestViewController alloc] init];
    //                    self.window.rootViewController = View;
    //
    //                    RuntimeViewController *runtimaView = [[RuntimeViewController alloc] init];
    //                    self.window.rootViewController = runtimaView;
    
    
    
    //                    TableViewController *view = [[TableViewController alloc] init];
    //                    self.window.rootViewController = view;
    
    //                }
    //                break;
    //
    //            case 1101:{
    //                    NSLog(@"点击跳过回调");
    //                    TableViewController *view = [[TableViewController alloc] init];
    //                    self.window.rootViewController = view;
    //                }
    //                break;
    //
    //            case 1102:{
    //                    NSLog(@"倒计时完成后的回调");
    //                    TableViewController *view = [[TableViewController alloc] init];
    //                    self.window.rootViewController = view;
    //                }
    //                break;
    //                
    //            default:
    //                break;
    //        }
    //    };
}

- (void)applicationWillResignActive:(UIApplication *)application {
    
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    [[UIApplication sharedApplication]setApplicationIconBadgeNumber:0];//进入前台取消应用消息图标
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    
}

- (void)applicationWillTerminate:(UIApplication *)application {

}

@end
