//
//  TwoTimesArray.h
//  DCRequestTest
//
//  Created by Joinwe on 16/5/30.
//  Copyright © 2016年 Joinwe. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TwoTimesArray : NSObject

@property (assign ,nonatomic) NSUInteger count;
- (void)incrementCount;
- (NSUInteger)countOfNumbers;
- (id)objectInNumbersAtIndex:(NSUInteger)index;

@end
