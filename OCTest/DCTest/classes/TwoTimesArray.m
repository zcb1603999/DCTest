//
//  TwoTimesArray.m
//  DCRequestTest
//
//  Created by Joinwe on 16/5/30.
//  Copyright © 2016年 Joinwe. All rights reserved.
//

#import "TwoTimesArray.h"

@implementation TwoTimesArray

- (NSUInteger)countOfNumbers {
    return  self.count;
}
- (id)objectInNumbersAtIndex:(NSUInteger)index {
    return @(index * 2);
}
- (void)incrementCount{
    self.count++;
}

@end
