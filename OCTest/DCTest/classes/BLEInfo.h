//
//  BLEInfo.h
//  DCRequestTest
//
//  Created by Joinwe on 16/6/3.
//  Copyright © 2016年 Joinwe. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>

@interface BLEInfo : NSObject

@property (nonatomic, retain) CBPeripheral *discoveredPeripheral;

@property (nonatomic, retain) NSNumber *rssi;

@end
