//
//  ThreeTimesArray.h
//  DCRequestTest
//
//  Created by Joinwe on 16/5/30.
//  Copyright © 2016年 Joinwe. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ThreeTimesArray : NSObject

@property (strong ,nonatomic) NSMutableArray *numbers;
- (void)insertObject:(id)object inNumbersAtIndex:(NSUInteger)index;
- (void)removeObjectFromNumbersAtIndex:(NSUInteger)index;

@end
