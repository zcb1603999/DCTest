//
//  UIView+Masonry_LJC.h
//  DCTest
//
//  Created by Joinwe on 16/6/24.
//  Copyright © 2016年 Joinwe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Masonry_LJC)

- (void) distributeSpacingHorizontallyWith:(NSArray*)views;

- (void) distributeSpacingVerticallyWith:(NSArray*)views;

@end
