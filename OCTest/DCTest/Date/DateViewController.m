//
//  DateViewController.m
//  DCTest
//
//  Created by Joinwe on 16/6/23.
//  Copyright © 2016年 Joinwe. All rights reserved.
//

#import "DateViewController.h"

@interface DateViewController ()

@end

@implementation DateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self testDate];
}

- (void)testDate{
    NSString *timeStr = @"2016-10-11 12:12:12";
    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    //设置你想要的格式,hh与HH的区别:分别表示12小时制,24小时制
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    
    NSTimeZone* timeZone = [NSTimeZone timeZoneWithName:@"Asia/Shanghai"];
    [formatter setTimeZone:timeZone];
    
    //将字符串按formatter转成nsdate
    NSDate* date = [formatter dateFromString:timeStr];
    
    NSLog(@"===========格式化后时间为%@",date);
    
    //现在时间,你可以输出来看下是什么格式
    NSDate *datenow = [NSDate date];
    
    //将nsdate按formatter格式转成nsstring
    NSString *nowtimeStr = [formatter stringFromDate:datenow];
    
    NSLog(@"===========当前时间为%@",nowtimeStr);
    
    
    
    NSString *time= @"1466663720";
    
    //时间转时间戳的方法:也就是获取当前时间的时间戳
    NSString *timeSp = [NSString stringWithFormat:@"%ld", (long)[datenow timeIntervalSince1970]];
    NSLog(@"timeSp:%@",timeSp); //时间戳的值
    
    //    时间戳转时间的方法
    NSDate *confromTimesp = [NSDate dateWithTimeIntervalSince1970:1466663720];
    NSLog(@"1466663720  = %@",confromTimesp);
    
    NSString *confromTimespStr = [formatter stringFromDate:confromTimesp];
    NSLog(@"confromTimespStr =  %@",confromTimespStr);
    
    
    //    时间戳转时间的方法:
    NSDateFormatter* format = [[NSDateFormatter alloc] init];
    [format setDateStyle:NSDateFormatterMediumStyle];
    [format setTimeStyle:NSDateFormatterShortStyle];
    [format setDateFormat:@"yyyyMMddHHMMss"];
    
    
    NSDate *date1 = [formatter dateFromString:time];
    NSLog(@"date1:%@",date1);
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

@end
