//
//  UINavigationController+DCBackGesture.h
//

/**
 *
 *  简单实现导航push到下一个页面后右滑返回前一个页面
 *
 *
 */



#import <UIKit/UIKit.h>

/**大于80显示前一个页面*/
#define BackGestureOffsetXToBack 80

@interface UINavigationController (DCBackGesture)<UIGestureRecognizerDelegate>

/**
 * @brief 默认是 NO
 * @note  如果在加载之后其他方面不工作了就调用这个属性
 */
@property (assign, nonatomic) BOOL enableBackGesture;

@end
