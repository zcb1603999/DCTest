//
//  DCSingle.h
//  tarbarTest
//
//  Created by Joinwe on 16/6/3.
//  Copyright © 2016年 Joinwe. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DCSingle : NSObject

//是否是pop
@property (nonatomic, assign) BOOL isPopSingle;

+ (DCSingle *)sharedInstance;

@end
