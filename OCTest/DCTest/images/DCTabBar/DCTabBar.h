//
//  DCTabBar.h
//  

#import <UIKit/UIKit.h>

@class DCTabBar;

@protocol DCTabBarDelegate <NSObject>

/**切换ViewController*/
- (void)tabBar:(DCTabBar *)tabBar didSelectedButtonFrom:(NSInteger)from to:(NSInteger)to;

@end




@interface DCTabBar : UIView

/**添加元素*/
- (void)addTabBarButtonWithItem:(UITabBarItem *)item;

@property (nonatomic, assign) NSInteger tabBarIndex;

/**代理*/
@property (nonatomic, weak) id<DCTabBarDelegate> delegate;

@end
