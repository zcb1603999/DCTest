//
//  DCTabBarButton.m
//  

#import "DCTabBarButton.h"
#import "DCBadgeButton.h"

//图标比例
#define DCTabBarButtonImageRatio 0.6

#define DCColor(r, g, b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1.0]

// 按钮的默认文字颜色
#define  DCTabBarButtonTitleColor  DCColor(116, 116, 116)

// 按钮的选中文字颜色
#define  DCTabBarButtonTitleSelectedColor  DCColor(250, 35, 80)

@interface DCTabBarButton()

/**提醒数字*/
@property (nonatomic, weak) DCBadgeButton *badgeButton;
@end


@implementation DCTabBarButton

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
//        self.backgroundColor = [UIColor yellowColor];
        
        //图标居中
//        self.imageView.backgroundColor = [UIColor purpleColor];
        self.imageView.contentMode = UIViewContentModeCenter;
        //文字居中
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        //文字大小
        self.titleLabel.font = [UIFont systemFontOfSize:13];
        //文字颜色
        [self setTitleColor:DCTabBarButtonTitleColor forState:UIControlStateNormal];
        [self setTitleColor:DCTabBarButtonTitleSelectedColor forState:UIControlStateSelected];
        //添加一个提醒数字按钮
//        DCBadgeButton *badgeButton = [[DCBadgeButton alloc] init];
//        badgeButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin;
//        [self addSubview:badgeButton];
//        self.badgeButton = badgeButton;
    }
    return self;
}

/**重写去掉高亮状态*/
- (void)setHighlighted:(BOOL)highlighted{}


/**内部图片的frame*/
- (CGRect)imageRectForContentRect:(CGRect)contentRect{
    CGFloat imageX = contentRect.origin.x;
    CGFloat imageY = contentRect.origin.y;
    CGFloat imageW = contentRect.size.width;
    CGFloat imageH = contentRect.size.height * DCTabBarButtonImageRatio;
    return CGRectMake(imageX, imageY, imageW, imageH);
}

/**内部文字的frame*/
- (CGRect)titleRectForContentRect:(CGRect)contentRect{
    
    CGFloat titleW = contentRect.size.width;
    CGFloat titleH = contentRect.size.height * DCTabBarButtonImageRatio;
    CGFloat titleX = contentRect.origin.x;
    CGFloat titleY = contentRect.size.height - titleH;
    
    return CGRectMake(titleX, titleY, titleW, titleH);
}


/**属性item的 setter && getter*/
- (void)setItem:(UITabBarItem *)item{
    _item = item;
    
    //KVO监听属性的变化
    //[item addObserver:self forKeyPath:@"badgeValue" options:0 context:nil];
    [item addObserver:self forKeyPath:@"title" options:0 context:nil];
    [item addObserver:self forKeyPath:@"image" options:0 context:nil];
    [item addObserver:self forKeyPath:@"selectedImage" options:0 context:nil];
    
    [self observeValueForKeyPath:nil ofObject:nil change:nil context:nil];
    
}

- (void)dealloc
{
    //[self.item removeObserver:self forKeyPath:@"badgeValue"];
    [self.item removeObserver:self forKeyPath:@"title"];
    [self.item removeObserver:self forKeyPath:@"image"];
    [self.item removeObserver:self forKeyPath:@"selectedImage"];
}

/**
 *  监听到某个对象的值变化了就会调用
 *
 *  @param keyPath 属性名
 *  @param object  哪个对象的属性被改变
 *  @param change  属性发生的变化
 *  @param context 上下文
 */
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context{
    //设置文字
    [self setTitle:self.item.title forState:UIControlStateNormal];
    [self setTitle:self.item.title forState:UIControlStateSelected];
    
    //设置图片
    [self setImage:self.item.image forState:UIControlStateNormal];
    [self setImage:self.item.image forState:UIControlStateSelected];
    
    
    //设置提醒数字
//    self.badgeButton.badgeValue = self.item.badgeValue;
    
    //设置提醒数字位置
//    CGFloat badgeY = 5;
//    CGFloat badgeX = self.frame.size.width - self.badgeButton.frame.size.width - 10;
//    CGRect badgeF = self.badgeButton.frame;
//    badgeF.origin.x = badgeX;
//    badgeF.origin.y = badgeY;
//    self.badgeButton.frame = badgeF;
    
    
}



@end
