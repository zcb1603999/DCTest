//
//  UIColor+DCColor.m
//  tarbarTest
//
//  Created by Joinwe on 15/12/8.
//  Copyright © 2015年 Joinwe. All rights reserved.
//

#import "UIColor+DCColor.h"

@implementation UIColor (DCColor)


+ (UIColor *)colorWithR:(CGFloat)r G:(CGFloat)g B:(CGFloat)b{
    return [UIColor colorWithR:r G:g B:b A:1.0f];
}


+ (UIColor *)colorWithR:(CGFloat)r G:(CGFloat)g B:(CGFloat)b A:(CGFloat)a{
    return [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a];
}


+ (UIColor *)colorWithHexColor:(NSString *)hexColor{
    unsigned int red, green, blue;
    
    NSRange range;
    
    range. length = 2 ;
    
    range. location = 0 ;
    
    [[ NSScanner scannerWithString :[hexColor substringWithRange :range]] scanHexInt :&red];
    
    range. location = 2 ;
    
    [[ NSScanner scannerWithString :[hexColor substringWithRange :range]] scanHexInt :&green];
    
    range. location = 4 ;
    
    [[ NSScanner scannerWithString :[hexColor substringWithRange :range]] scanHexInt :&blue];
    
//    return [ UIColor colorWithRed :( float )(red/ 255.0f ) green :( float )(green/ 255.0f ) blue :( float )(blue/ 255.0f ) alpha : 1.0f ];
    
    return [UIColor colorWithR:(float)red G:(float)green B:(float)green];
}

@end
