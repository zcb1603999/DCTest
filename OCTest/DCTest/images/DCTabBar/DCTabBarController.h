//
//  DCTabBarController.h
//


/**
 *  
 *  自定义TabBar
 *
 *  希望在外部设置子视图，并做一些简单设置
 *
 */




#import <UIKit/UIKit.h>
#import "DCTabBar.h"

@interface DCTabBarController : UITabBarController

/**自定义的tabbar*/
@property (nonatomic, weak) DCTabBar *customTabBar;

@property(nonatomic, assign) NSInteger tabBarIndex;

@end
