//
//  DCSingle.m
//  tarbarTest
//
//  Created by Joinwe on 16/6/3.
//  Copyright © 2016年 Joinwe. All rights reserved.
//

#import "DCSingle.h"

static DCSingle  *_instance;

@implementation DCSingle

+ (DCSingle *)sharedInstance{
    if (_instance==nil) {
        _instance=[[DCSingle alloc]init];
    }
    return _instance;
}

@end
