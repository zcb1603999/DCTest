//
//  UIColor+DCColor.h
//  tarbarTest
//
//  Created by Joinwe on 15/12/8.
//  Copyright © 2015年 Joinwe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (DCColor)


/**
*  rgb颜色转UIColor
*
*  @param r 0~255
*  @param g 0~255
*  @param b 0~255
*
*  @return UIColor
*/
+ (UIColor *)colorWithR:(CGFloat)r G:(CGFloat)g B:(CGFloat)b;

/**
 *  rgb颜色转UIColor
 *
 *  @param r 0~255
 *  @param g 0~255
 *  @param b 0~255
 *  @param a 0~1.0
 *
 *  @return UIColor
 */
+ (UIColor *)colorWithR:(CGFloat)r G:(CGFloat)g B:(CGFloat)b A:(CGFloat)a;

/**
 *  16进制颜色转UIColor
 *
 *  @param hexColor like #ffffff
 *
 *  @return UIColor
 */
+ (UIColor *)colorWithHexColor:(NSString *)hexColor;

@end
