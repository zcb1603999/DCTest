//
//  DCTabBarButton.h
//  

#import <UIKit/UIKit.h>

@interface DCTabBarButton : UIButton
/**按钮元素*/
@property (strong, nonatomic) UITabBarItem *item;


/**设置标题文字颜色*/
@property (strong, nonatomic) UIColor *titleColor;

/**设置选中标题文字颜色*/
@property (strong, nonatomic) UIColor *titleSelectedColor;



@end
