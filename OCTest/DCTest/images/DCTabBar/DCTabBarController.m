//
//  DCTabBarController.m
//  

#import "DCTabBarController.h"
#import "ViewController.h"
#import "DCViewController.h"
#import "DCSingle.h"
#import "UINavigationController+DCBackGesture.h"
#import <FDFullscreenPopGesture/UINavigationController+FDFullscreenPopGesture.h>
#import "DCNavigationController.h"

@interface DCTabBarController ()<DCTabBarDelegate>

@end

@implementation DCTabBarController


- (void)viewDidLoad {
    [super viewDidLoad];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeSubView) name:@"popToRootViewController" object:nil];
    
    [self initTabBar];
    
    [self initChildViewControllers];
    
}

//防止popToRootViewController的时候出现重影
- (void)removeSubView{
    for (UIView *childV in self.tabBar.subviews) {
        if ([childV isKindOfClass:[UIControl class]]) {
            [childV removeFromSuperview];
        }
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //删除系统自动生成的UITabBarButton
    for (UIView *child in self.tabBar.subviews) {
        if ([child isKindOfClass:[UIControl class]]) {
            [child removeFromSuperview];
        }
    }
}


/**初始化tabbar*/
- (void)initTabBar{
    DCTabBar *customTabBar = [[DCTabBar alloc] init];
    customTabBar.frame = self.tabBar.bounds;
    customTabBar.delegate = self;
    [self.tabBar addSubview:customTabBar];
    self.customTabBar = customTabBar;
}

/**初始化所有的子控制器*/
- (void)initChildViewControllers{
    
    ViewController *viewController = [[ViewController alloc] init];
    [self setupChildViewController:viewController title:@"test" imageName:@"icon_home_pressed@3x" selectedImageName:@"icon_home_pressed@3x"];
    
    DCViewController *dcViewController = [[DCViewController alloc] init];
    [self setupChildViewController:dcViewController title:@"test1" imageName:@"icon_home_pressed@3x" selectedImageName:@"icon_home_pressed@3x"];
    
    ViewController *viewController1 = [[ViewController alloc] init];
    [self setupChildViewController:viewController1 title:@"test2" imageName:@"icon_home_pressed@3x" selectedImageName:@"icon_home_pressed@3x"];
    
    DCViewController *dcViewController1 = [[DCViewController alloc] init];
    [self setupChildViewController:dcViewController1 title:@"test1" imageName:@"icon_home_pressed@3x" selectedImageName:@"icon_home_pressed@3x"];
    
}


- (void)setupChildViewController:(UIViewController *)childVC title:(NSString *)title imageName:(NSString *)imageName selectedImageName:(NSString *)selectedImageName{
    //1.设置控制器属性
    childVC.tabBarItem.title = title;
        //设置图标
    childVC.tabBarItem.image = [UIImage imageNamed:imageName];
        //设置选中的图标
    childVC.tabBarItem.selectedImage = [[UIImage imageNamed:selectedImageName] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    //2.包装一个导航控制器
    DCNavigationController *nav = [[DCNavigationController alloc] initWithRootViewController:childVC];
    nav.navigationBar.barTintColor = [UIColor purpleColor];
//    nav.enableBackGesture = YES;
    nav.fd_fullscreenPopGestureRecognizer.enabled = YES;
    [self addChildViewController:nav];
    
    //3.添加tabbar内部按钮
    [self.customTabBar addTabBarButtonWithItem:childVC.tabBarItem];

}

/**
 *  监听按钮的改变
 *
 *  @param tabBar tabbar
 *  @param from   原来选中的位置
 *  @param to     最新选中的位置
 */
- (void)tabBar:(DCTabBar *)tabBar didSelectedButtonFrom:(NSInteger)from to:(NSInteger)to{
    self.selectedIndex = to;
}


- (void)setTabBarIndex:(NSInteger)tabBarIndex{
    _tabBarIndex = tabBarIndex;
    if (tabBarIndex != 0) {
        self.customTabBar.tabBarIndex = tabBarIndex;
        [self tabBar:self.customTabBar didSelectedButtonFrom:0 to:(int)tabBarIndex];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



@end
