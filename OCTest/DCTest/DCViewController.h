//
//  DCViewController.h
//  tarbarTest
//
//  Created by Joinwe on 15/12/8.
//  Copyright © 2015年 Joinwe. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kWeakObject(obj) __weak __typeof(obj) weakObject = obj;
#define kIOSVersion ((float)[[[UIDevice currentDevice] systemVersion] doubleValue])

@interface DCViewController : UIViewController

- (instancetype)initWithTitle:(NSString *)title;

@end
