//
//  UIControl+DCControl.m
//  DCTest
//
//  Created by Joinwe on 16/6/28.
//  Copyright © 2016年 Joinwe. All rights reserved.
//

#import "UIControl+DCControl.h"
#import <objc/runtime.h>

static const void *kDCTouchUpBlockKey = "kDCTouchUpBlockKey";

@implementation UIControl (DCControl)

- (void)setDc_touchUpBlock:(DCTouchUpBlock)dc_touchUpBlock{
    objc_setAssociatedObject(self,
                             kDCTouchUpBlockKey,
                             dc_touchUpBlock,
                             OBJC_ASSOCIATION_COPY);
    
    
    [self removeTarget:self
                action:@selector(dcOnTouchUp:)
                forControlEvents:UIControlEventTouchUpInside];
    
    if (dc_touchUpBlock) {
        [self addTarget:self
                 action:@selector(dcOnTouchUp:)
                 forControlEvents:UIControlEventTouchUpInside];
    }
}

- (DCTouchUpBlock)dc_touchUpBlock {
    return objc_getAssociatedObject(self, kDCTouchUpBlockKey);
}


- (void)dcOnTouchUp:(UIButton *)sender {
    DCTouchUpBlock touchUp = self.dc_touchUpBlock;
    if (touchUp) {
        touchUp(sender);
    }
}


@end
