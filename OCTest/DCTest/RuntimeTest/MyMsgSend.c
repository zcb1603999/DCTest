//
//  MyMsgSend.c
//  DCTest
//
//  Created by Joinwe on 16/6/29.
//  Copyright © 2016年 Joinwe. All rights reserved.
//

#include "MyMsgSend.h"


static const void *myMsgSend(id receiver, const char *name){
    SEL selector = sel_registerName(name);
    IMP methodIMP = class_getMethodImplementation(objc_getClass(receiver), selector);
    return methodIMP(receiver, selector);;
}


void RunMyMsgSend(){
    //NSObject *object = [NSObject alloc]]init;
    Class class = (Class)objc_getClass("NSObject");
    id object = class_createInstance(class, 0);
    myMsgSend(object, "init");
    
    //id description = [object description];
    id description = (id)myMsgSend(object, "description");
    
    //const char *cstr = [description UTF8String];
    const char *cstr = myMsgSend(description, "UTF8String");
    
    printf("%s\n", cstr);
}