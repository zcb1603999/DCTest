//
//  People.h
//  DCTest
//
//  Created by Joinwe on 16/6/30.
//  Copyright © 2016年 Joinwe. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface People : NSObject

@property (copy) NSString *givenName;
@property (copy) NSString *surnName;

@end
