//
//  RuntimeViewController.m
//  DCTest
//
//  Created by Joinwe on 16/6/28.
//  Copyright © 2016年 Joinwe. All rights reserved.
//

#import "RuntimeViewController.h"
#import <objc/objc.h>
#import <objc/runtime.h>
#import "UIControl+DCControl.h"
//#import "Person.h"

@interface RuntimeViewController ()

@end

@implementation RuntimeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor greenColor];

    [self msgSendTest];
}

- (void)msgSendTest{
//    Person *person = [[Person alloc] init];
//    [person setGivenName:@"Bob"];
//    [person setSurnName:@"Jones"];
//    
//    NSLog(@"%@,%@",[person givenName],[person surnName]);
}

- (void)getListTest{
    unsigned int count;
    //获取属性列表
    objc_property_t *propertyList = class_copyPropertyList([NSObject class], &count);
    for (unsigned int i=0; i<count; i++) {
        const char *propertyName = property_getName(propertyList[i]);
        NSLog(@"property---->%@", [NSString stringWithUTF8String:propertyName]);
    }
    free(propertyList);
    
    //获取方法列表
    Method *methodList = class_copyMethodList([NSObject class], &count);
    for (unsigned int i; i<count; i++) {
        Method method = methodList[i];
        NSLog(@"method---->%@", NSStringFromSelector(method_getName(method)));
    }
    //运行时环境没有引用计数，所以没有等价的retain或者release方法，如果从带有copy的函数得到一个值，就应该free，如果用了不带copy单词的函数，千万不要调用free。
    free(methodList);
    
    //获取成员变量列表
    Ivar *ivarList = class_copyIvarList([NSObject class], &count);
    for (unsigned int i; i<count; i++) {
        Ivar myIvar = ivarList[i];
        const char *ivarName = ivar_getName(myIvar);
        NSLog(@"Ivar---->%@", [NSString stringWithUTF8String:ivarName]);
    }
    free(ivarList);
    
    //获取协议列表
    __unsafe_unretained Protocol **protocolList = class_copyProtocolList([NSObject class], &count);
    for (unsigned int i; i<count; i++) {
        Protocol *myProtocal = protocolList[i];
        const char *protocolName = protocol_getName(myProtocal);
        NSLog(@"protocol---->%@", [NSString stringWithUTF8String:protocolName]);
    }
    free(protocolList);
}


- (void)DCControlTest{
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(20, 40, 280, 50)];
    button.backgroundColor = [UIColor blueColor];
    [button setTitle:@"点我啊" forState:UIControlStateNormal];
    [self.view addSubview:button];
    
    button.dc_touchUpBlock = ^(UIButton *sender){
        NSLog(@"%@",sender.titleLabel.text);
    };
}


- (void)runTimeTest{
    static char overviewKey;
    NSArray *array = [[NSArray alloc] initWithObjects:@"测试1",@"测试2",@"测试3", nil];
    NSString *overview = @"浮点数";
    
    objc_setAssociatedObject(array, &overviewKey, overview, OBJC_ASSOCIATION_RETAIN);
    
    
    NSString *associatedObject = (NSString *)objc_getAssociatedObject(array, &overviewKey);
    
    NSLog(@"%@",associatedObject);
    
    
    //断开关联
    objc_setAssociatedObject(array, &overviewKey, nil, OBJC_ASSOCIATION_ASSIGN);
    
    //下面的也可以断开连接，但是不建议使用，因为他会断开所有关联。只有在需要把对象恢复到“原始状态”的时候才会使用这个函数。
    //objc_removeAssociatedObjects(array);
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
