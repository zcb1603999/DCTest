//
//  UIControl+DCControl.h
//  DCTest
//
//  Created by Joinwe on 16/6/28.
//  Copyright © 2016年 Joinwe. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^DCTouchUpBlock)(id sender);

@interface UIControl (DCControl)

@property (nonatomic, copy) DCTouchUpBlock dc_touchUpBlock;

@end
