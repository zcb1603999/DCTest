//
//  DCWaterViewController.m
//  DCTest
//
//  Created by Joinwe on 16/6/21.
//  Copyright © 2016年 Joinwe. All rights reserved.
//

#import "DCWaterViewController.h"
#import "DCSimpleWaterLayout.h"
#import "DCDefaultWaterLayout.h"

#import "AvatarCollectionViewCell.h"                        //自定义cell
#import "Model.h"                             //Model类型
#define kFileName  @"Data"                    //文件名
#define kFileType  @"json"                    //文件类型
#define kItemWidth ([UIScreen mainScreen].bounds.size.width / 2 - 30) //item的宽度

#define kDeviceSize [UIScreen mainScreen].bounds.size

#define kUICollectionViewCell @"cell"

@interface DCWaterViewController ()<UICollectionViewDataSource,UICollectionViewDelegate, DCDefaultWaterLayoutDelegate,DCSimpleWaterLayoutDelegate>{
    UICollectionView *collectionview;
}
@property (nonatomic, retain) NSMutableArray *dataArr;
@property (nonatomic, retain) UICollectionView *collectionView;
@end

@implementation DCWaterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor blueColor];
    
    [self parserData];
    [self createCollection];
}

#pragma mark - handle data source
- (void)parserData {
    //1.获取文件路径
    NSString *filePath = [[NSBundle mainBundle] pathForResource:kFileName ofType:kFileType];
    //2.初始化NSData对象
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    //3.JSON解析
    NSArray *resultArr = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    //4.将数组中的字典封装成Model
    
    for (NSDictionary *dic in resultArr) {
        Model *model = [Model modelWithDictionary:dic];
        [self.dataArr addObject:model];
    }

    //5.集合视图重新刷新
    [self.collectionView reloadData];
}

#pragma mark - lazy loading
- (NSMutableArray *)dataArr {
    if (!_dataArr) {
        self.dataArr = [NSMutableArray arrayWithCapacity:0];
    }
    return _dataArr;
}

- (void)createCollection{
    /**
     布局瀑布流效果, 要自定义布局样式, 继承自UICollectionViewLayout
     DCDefaultWaterLayout 是第三方的一个瀑布流样式
     */
    //    UICollectionViewFlowLayout
    DCDefaultWaterLayout *waterFlow = [[DCDefaultWaterLayout alloc] init];
    //1.设置item的宽度
    waterFlow.itemWidth = kItemWidth;
    //2.设置每个分区的缩进量
    waterFlow.sectionInset = UIEdgeInsetsMake(5, 5, 10, 5);
    //3.设置代理, 用来动态返回每一个item的高度
    waterFlow.delegate = self;
    //4.设置最小行间距
    waterFlow.minLineSpacing = 10;
    
    
    self.collectionView = [[UICollectionView alloc] initWithFrame:[UIScreen mainScreen].bounds collectionViewLayout:waterFlow];
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    self.collectionView.backgroundColor = [UIColor blueColor];
    [self.view addSubview:self.collectionView];
    
    [self.collectionView registerClass:[AvatarCollectionViewCell class] forCellWithReuseIdentifier:kUICollectionViewCell];
}


- (void)createCollectionView{
    DCSimpleWaterLayout *waterLayout = [[DCSimpleWaterLayout alloc] init];
    waterLayout.delegate = self;
    collectionview = [[UICollectionView alloc] initWithFrame:[UIScreen mainScreen].bounds collectionViewLayout:waterLayout];
    // 配置属性
    collectionview.backgroundColor = [UIColor lightGrayColor];
    // 设置数据源代理
    collectionview.dataSource = self;
    //配置代理对象
    collectionview.delegate = self;
    // 添加到主视图
    [self.view addSubview:collectionview];
    
    [collectionview registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:kUICollectionViewCell];
}

#pragma mark - UICollectionViewDataSource
//设置分区的个数
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

//设置分区item的个数
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataArr.count;
//    return 100;
}

//针对于每个item返回cell对象
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
//    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kUICollectionViewCell
//                                                                           forIndexPath:indexPath];
//    cell.backgroundColor = [UIColor redColor];
    
    AvatarCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kUICollectionViewCell forIndexPath:indexPath];
    [cell configureCellWithModel:self.dataArr[indexPath.row]];
    return cell;
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
//    CGFloat randomHeight = 100 + (arc4random() % 140);
//    return CGSizeMake(100, randomHeight); // 100 to 240 pixels tall
    
    //根据对应的Model对象, 动态计算出每个item的高度,
    //按比例进行缩放,得到最终缩放之后的高度,返回
    Model *model = self.dataArr[indexPath.row];
    CGFloat randomHeight = kItemWidth / model.width.floatValue * model.height.floatValue;
    return CGSizeMake(100, randomHeight);
}

//item选中事件
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
}

//动态返回每个item的高度 DCSimpleWaterLayout || DCDefaultWaterLayout
- (CGFloat) collectionView:(UICollectionView *) collectionView layout:(DCDefaultWaterLayout *) layout heightForItemAtIndexPath:(NSIndexPath *) indexPath {
    
    // we will use a random height from 100 - 400
//    CGFloat randomHeight = 100 + (arc4random() % 140);
//    return randomHeight;
    
    
    //根据对应的Model对象, 动态计算出每个item的高度,
    //按比例进行缩放,得到最终缩放之后的高度,返回
    Model *model = self.dataArr[indexPath.row];
    CGFloat randomHeight = kItemWidth / model.width.floatValue * model.height.floatValue;
    
    NSLog(@"=====%f",randomHeight);
    
    return randomHeight;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
