//
//  DCDefaultWaterLayout.h
//  DCTest
//
//  Created by Joinwe on 16/6/21.
//  Copyright © 2016年 Joinwe. All rights reserved.
//

#import <UIKit/UIKit.h>
@class DCDefaultWaterLayout;

@protocol DCDefaultWaterLayoutDelegate <UICollectionViewDelegateFlowLayout>
@required
//每个cell高度指定代理
- (CGFloat) collectionView:(UICollectionView *) collectionView
                    layout:(DCDefaultWaterLayout *) layout
  heightForItemAtIndexPath:(NSIndexPath *) indexPath;
@end

@interface DCDefaultWaterLayout : UICollectionViewLayout

@property (nonatomic, assign) NSUInteger columnCount; // 列数
@property (nonatomic, assign) CGFloat itemWidth; // item的宽度
@property (nonatomic, assign) UIEdgeInsets sectionInset; // 每个section的边框间距
@property (nonatomic, assign) CGFloat minLineSpacing;  //每行每列的间隔

/**代理**/
@property (weak, nonatomic) id<DCDefaultWaterLayoutDelegate> delegate;
@end
