//
//  Model.h
//  DCTest
//
//  Created by Joinwe on 16/6/21.
//  Copyright © 2016年 Joinwe. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Model : NSObject
/**
 *
 {
 "thumbURL": "http://amuse.nen.com.cn/imagelist/11/21/9as70n3ir61b.jpg",
 "width": 482,
 "height": 480
 },
 */
@property (nonatomic, copy) NSString *thumbURL;
@property (nonatomic, retain) NSNumber *width;
@property (nonatomic, retain) NSNumber *height;

- (id)initWithDictionary:(NSDictionary *)dictionary;
+ (id)modelWithDictionary:(NSDictionary *)dictionary;
@end
