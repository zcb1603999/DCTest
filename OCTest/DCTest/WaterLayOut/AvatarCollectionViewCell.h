//
//  AvatarCollectionViewCell.h
//  DCTest
//
//  Created by Joinwe on 16/6/21.
//  Copyright © 2016年 Joinwe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Model.h"

@interface AvatarCollectionViewCell : UICollectionViewCell
//imageView --- 展示头像
@property (nonatomic, retain) UIImageView *imageView;
- (void)configureCellWithModel:(Model *)model;
@end
