//
//  AvatarCollectionViewCell.m
//  DCTest
//
//  Created by Joinwe on 16/6/21.
//  Copyright © 2016年 Joinwe. All rights reserved.
//

#import "AvatarCollectionViewCell.h"

#import <SDWebImage/UIImageView+WebCache.h>

@implementation AvatarCollectionViewCell
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        //添加子控件
        [self.contentView addSubview:self.imageView];
    }
    return self;
}
//lazy loading
- (UIImageView *)imageView {
    if (!_imageView) {
        self.imageView = [[UIImageView alloc] initWithFrame:self.bounds];
        _imageView.backgroundColor = [UIColor lightGrayColor];
    }
    //在getter方法中,千万不要写self.imageView取值,会形成死循环.
//    return [[_imageView retain] autorelease];
    return _imageView;
}
//1.当视图大小发生变化时,触发.
//2.当添加到父视图时触发.
//3.当屏幕旋转时触发.
//修改子视图的大小
- (void)layoutSubviews {
    [super layoutSubviews];
    self.imageView.frame = self.bounds;
}
- (void)configureCellWithModel:(Model *)model {
    [self.imageView sd_setImageWithURL:[NSURL URLWithString:model.thumbURL] placeholderImage:[UIImage imageNamed:@"2.jpg"]];
}
@end
