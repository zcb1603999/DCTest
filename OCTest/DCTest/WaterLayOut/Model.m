//
//  Model.m
//  DCTest
//
//  Created by Joinwe on 16/6/21.
//  Copyright © 2016年 Joinwe. All rights reserved.
//

#import "Model.h"

@implementation Model
- (id)initWithDictionary:(NSDictionary *)dictionary {
    self = [super init];
    if (self) {
        [self setValuesForKeysWithDictionary:dictionary];
    }
    return self;
}
- (void)setValue:(id)value forUndefinedKey:(NSString *)key {
    
}


+ (id)modelWithDictionary:(NSDictionary *)dictionary {
    return [[Model alloc] initWithDictionary:dictionary];
}
@end
