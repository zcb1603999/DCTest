//
//  DCSimpleWaterLayout.h
//  DCTest
//
//  Created by Joinwe on 16/6/21.
//  Copyright © 2016年 Joinwe. All rights reserved.
//

#import <UIKit/UIKit.h>
@class DCSimpleWaterLayout;

@protocol DCSimpleWaterLayoutDelegate <UICollectionViewDelegateFlowLayout>
@required
//每个cell高度指定代理
- (CGFloat) collectionView:(UICollectionView *) collectionView
                    layout:(DCSimpleWaterLayout *) layout
  heightForItemAtIndexPath:(NSIndexPath *) indexPath;
@end

@interface DCSimpleWaterLayout : UICollectionViewLayout

@property (nonatomic, assign) NSUInteger numberOfColumns;
@property (nonatomic, assign) CGFloat interItemSpacing;

/**代理**/
@property (weak, nonatomic) id<DCSimpleWaterLayoutDelegate> delegate;
@end
