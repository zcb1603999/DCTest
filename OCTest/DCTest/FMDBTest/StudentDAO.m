//
//  StudentDAO.m
//  DCTest
//
//  Created by Joinwe on 16/6/27.
//  Copyright © 2016年 Joinwe. All rights reserved.
//

#import "StudentDAO.h"
#import "CreateDB.h"


@implementation StudentDAO
+ (void)insertData:(Student *)student{
    //    CreateDB 类是用来创建数据库文件 包括表
    FMDatabase *db = [CreateDB createDB];
    
    if (![db open]){
        [db close];
        return;
    }
    [db executeUpdate:@"INSERT INTO student (name,num) VALUES (?,?)",student.s_name,[NSString stringWithFormat:@"%d",student.s_num]];
    [db close];
}


+ (NSMutableArray *)selectData{
    FMDatabase *db = [CreateDB createDB];
    
    if (![db open]){
        [db close];
        return nil;
    }
    
    FMResultSet *resultSet = [db executeQuery:@"SELECT * FROM student"];
    
    NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity:0];
    
    
    while ([resultSet next]){
        Student *student = [[Student alloc] init];
        
        student.s_name = [resultSet stringForColumn:@"name"];
        NSLog(@"=====%@",student.s_name);
        [array addObject:student];
        
    }
    [db close];
    
    return array;
}

//更改
+ (void)updateStudent:(Student *)student{
    FMDatabase *db = [CreateDB createDB];
    
    if (![db open]){
        [db close];
        return ;
    }
    
    [db executeUpdate:@"UPDATE student SET name=?,num=? WHERE id=?",[NSString stringWithFormat:@"%@",student.s_name],[NSString stringWithFormat:@"%d",student.s_num],[NSString stringWithFormat:@"%d",student.s_id]];
    [db close];
}

//删除
+ (void)deleteStudent:(Student *)student{
    FMDatabase *db = [CreateDB createDB];
    
    if (![db open]){
        [db close];
        return ;
    }
    [db executeUpdate:@"DELETE FROM student WHERE id=?",[NSString stringWithFormat:@"%d",student.s_id]];
    [db close];
}

@end
