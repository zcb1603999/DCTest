//
//  FMDBTestViewController.m
//  DCTest
//
//  Created by Joinwe on 16/6/27.
//  Copyright © 2016年 Joinwe. All rights reserved.
//

#import "FMDBTestViewController.h"
#import "Student.h"
#import "StudentDAO.h"

@interface FMDBTestViewController ()<UITableViewDataSource,UITableViewDelegate>{
    NSMutableArray *_stuArray;
    UITableView *_tableView;
    int _currentRow;
}

@end

@implementation FMDBTestViewController


/*
 
 使用FMDB框架
 1.把第三方库倒入工程中
 2.让倒入的.m文件支持arc(1.打开：你的target - Bulid Phases -Compile Sources。2.双击对应的*.m文件。3.在弹出的窗口中输入上面提到的标签 -fobjc-arc / -fno-objc-arc4.直接按Enter键保存)  输入 -fno-objc-arc
 3.到入系统类库 libsql3.0
 
 就可以使用了
 
 */


- (void)viewDidLoad {
    [super viewDidLoad];

    _currentRow = -10000000;
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 20, 320, 400) style:UITableViewStylePlain];
    _tableView.delegate =self;
    _tableView.dataSource = self;
    [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    [self.view addSubview:_tableView];
    
    
    _stuArray = [[NSMutableArray alloc] initWithCapacity:0];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _stuArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    Student *smallStudent = [_stuArray objectAtIndex:indexPath.row];
    cell.textLabel.text = [NSString stringWithFormat:@"%d--%@--%d",smallStudent.s_id,smallStudent.s_name,smallStudent.s_num];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    _currentRow = (int)indexPath.row;
}

//插入数据
- (IBAction)insertData:(id)sender {
    //    创建一个对象  有对象的属性
    
    Student *student = [[Student alloc] init];
    student.s_name = @"阿斯顿撒旦";
    student.s_num = 12;
    [StudentDAO insertData:student];
    
    
    _stuArray = [StudentDAO selectData];
    
    NSLog(@"==== %@",_stuArray);
    [_tableView reloadData];
    
}

//更新数据
- (IBAction)updateData:(id)sender {
    if (_currentRow < 0) {
        return;
    }
    Student *student = [_stuArray objectAtIndex:_currentRow];
    student.s_name = @"徐卫星";
    student.s_num = 100;
    [StudentDAO updateStudent:student];
    
    _stuArray = [StudentDAO selectData];
    
    NSLog(@"==== %@",_stuArray);
    [_tableView reloadData];
}


//删除数据
- (IBAction)deleteData:(id)sender {
    if (_currentRow < 0) {
        return;
    }
    Student *student = [_stuArray objectAtIndex:_currentRow];
    [StudentDAO deleteStudent:student];
    
    _stuArray = [StudentDAO selectData];
    
    NSLog(@"==== %ld",_stuArray.count);
    [_tableView reloadData];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
