//
//  CreateDB.h
//  DCTest
//
//  Created by Joinwe on 16/6/27.
//  Copyright © 2016年 Joinwe. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FMDB/FMDatabase.h>

@interface CreateDB : NSObject

/**用来创建数据库的对象**/
+ (FMDatabase *)createDB;

@end
