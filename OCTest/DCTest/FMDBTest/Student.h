//
//  Student.h
//  DCTest
//
//  Created by Joinwe on 16/6/27.
//  Copyright © 2016年 Joinwe. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Student : NSObject

@property int s_id;
@property NSString *s_name;
@property int s_num;

@end
