//
//  StudentDAO.h
//  DCTest
//
//  Created by Joinwe on 16/6/27.
//  Copyright © 2016年 Joinwe. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Student.h"


//这个类是操作数据库的类
@interface StudentDAO : NSObject

//插入数据
+ (void)insertData:(Student *)student;
//查询
+ (NSMutableArray *)selectData;
//更改
+ (void)updateStudent:(Student *)student;
//删除
+ (void)deleteStudent:(Student *)student;

@end
