//
//  CreateDB.m
//  DCTest
//
//  Created by Joinwe on 16/6/27.
//  Copyright © 2016年 Joinwe. All rights reserved.
//

#import "CreateDB.h"
#import "FMDatabaseAdditions.h"

@implementation CreateDB

/**用来创建数据库的对象**/
+ (FMDatabase *)createDB{
    //    NSFileManager *fileManager = [NSFileManager defaultManager];
    //    FMDatabase *db;
    //    if (![fileManager fileExistsAtPath:[NSHomeDirectory() stringByAppendingPathComponent:@"Documents/test.sqlite"]])
    //    {
    FMDatabase * db = [FMDatabase databaseWithPath:[NSHomeDirectory() stringByAppendingPathComponent:@"Documents/test.sqlite"]];
    
    NSLog(@"====%@",db);
    
    //    }
    if (![db open]){
        [db close];
        return nil;
    }
    
    if (![db tableExists:@"student"]){
        [db executeUpdate:@"CREATE TABLE student (id integer PRIMARY KEY autoincrement,name text,num integer)"];
    }
    return db;
}

@end
