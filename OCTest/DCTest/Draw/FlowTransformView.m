//
//  FlowTransformView.m
//  DCTest
//
//  Created by Joinwe on 16/6/17.
//  Copyright © 2016年 Joinwe. All rights reserved.
//

#import "FlowTransformView.h"

@implementation FlowTransformView

static inline CGAffineTransform
CGAffineTransformMakeScaleTranslate(CGFloat sx, CGFloat sy,
                                    CGFloat dx, CGFloat dy){
    return CGAffineTransformMake(sx, 0.f, 0.f, sy, dx, dy);
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    CGSize size = self.bounds.size;
    CGFloat margin = 10;
    
    [[UIColor greenColor] set];
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    
    [path addArcWithCenter:CGPointMake(0, -1)
                    radius:1
                    startAngle:-M_PI
                    endAngle:0
                    clockwise:YES];
    
    [path addArcWithCenter:CGPointMake(1, 0)
                    radius:1
                    startAngle:-M_PI_2
                    endAngle:M_PI_2
                    clockwise:YES];
    
    [path addArcWithCenter:CGPointMake(0, 1)
                    radius:1
                    startAngle:0
                    endAngle:M_PI
                    clockwise:YES];
    
    [path addArcWithCenter:CGPointMake(-1, 0)
                    radius:1
                    startAngle:M_PI_2
                    endAngle:-M_PI_2
                    clockwise:YES];
    [path closePath];
    
    
    
    CGFloat scale = floorf((MIN(size.height, size.width) - margin) / 4);
    CGAffineTransform transform;
    transform = CGAffineTransformMakeScaleTranslate(scale,
                                                    scale,
                                                    size.width/2,
                                                    size.height/2);
    [path applyTransform:transform];
    [path fill];
    
}

@end
