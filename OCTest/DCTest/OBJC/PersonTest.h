//
//  PersonTest.h
//  DCTest
//
//  Created by Joinwe on 16/8/15.
//  Copyright © 2016年 Joinwe. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PersonTest : NSObject

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *age;
@property (nonatomic, copy) NSString *sex;
@property (nonatomic, copy) NSString *height;
@property (nonatomic, copy) NSString *weight;


- (instancetype)initWithName:(NSString*)name age:(NSString*)age sex:(NSString*)sex;

@end
