//
//  PersonViewModel.h
//  MVVMTest
//
//  Created by Joinwe on 16/8/11.
//  Copyright © 2016年 Joinwe. All rights reserved.
//

#import <Foundation/Foundation.h>
@class PersonTest;


@interface PersonViewModel : NSObject


@property (nonatomic, readonly) PersonTest *person;

@property (nonatomic, readonly) NSString *nameText;

@property (nonatomic, readonly) NSString *ageText;

@property (nonatomic, readonly) NSString *sexText;


- (instancetype)initWithPerson:(PersonTest *)person;


@end
