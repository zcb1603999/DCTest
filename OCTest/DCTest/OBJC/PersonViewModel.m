//
//  PersonViewModel.m
//  MVVMTest
//
//  Created by Joinwe on 16/8/11.
//  Copyright © 2016年 Joinwe. All rights reserved.
//

#import "PersonViewModel.h"
#import "PersonTest.h"

@interface PersonViewModel()

@end



@implementation PersonViewModel

- (instancetype)initWithPerson:(PersonTest *)person{
    self = [super init];
    if (!self) return nil;
    
    _person = person;
    
    _nameText = [NSString stringWithFormat:@"姓名为：%@", self.person.name];
    
    _ageText = [NSString stringWithFormat:@"年龄为：%@",self.person.age];
    
    _sexText = [NSString stringWithFormat:@"性别为：%@",self.person.sex];
    
    return self;
}



@end
