//
//  PersonTest.m
//  DCTest
//
//  Created by Joinwe on 16/8/15.
//  Copyright © 2016年 Joinwe. All rights reserved.
//

#import "PersonTest.h"

@implementation PersonTest

- (instancetype)initWithName:(NSString*)name age:(NSString*)age sex:(NSString*)sex{
    self = [super init];
    if (!self) return nil;
    
    _name = name;
    
    _age = age;
    
    _sex = sex;
    
    return self;
}

@end
