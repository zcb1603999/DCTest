//
//  GCDGroup.h
//  DCTest
//
//  Created by Joinwe on 16/7/7.
//  Copyright © 2016年 Joinwe. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GCDGroup : NSObject

@property (nonatomic, strong, readonly) dispatch_group_t dispatchGroup;

- (instancetype)init;

- (void) enter;
- (void) leave;
- (void) wait;
- (BOOL) wait:(int64_t)delta;


@end
