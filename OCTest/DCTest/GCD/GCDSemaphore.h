//
//  GCDSemaphore.h
//  DCTest
//
//  Created by Joinwe on 16/7/7.
//  Copyright © 2016年 Joinwe. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GCDSemaphore : NSObject

@property (nonatomic, strong, readonly) dispatch_semaphore_t dispatchSemaphore;

- (instancetype)init;
- (instancetype)initWithValue:(long)value;

- (BOOL)signal;
- (void)wait;
- (BOOL)wait:(int64_t)delta;


@end
