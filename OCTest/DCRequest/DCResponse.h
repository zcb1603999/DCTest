/**
 *  请求的返回
 */

#import <Foundation/Foundation.h>

@interface DCResponse : NSObject

/**
 *  请求返回结果数据
 */
@property NSDictionary *data;

/**
 *  请求结果码
 */
@property int code;

/**
 *  消息
 */
@property NSString *message;


@end
