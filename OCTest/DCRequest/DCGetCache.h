

#import <Foundation/Foundation.h>

@interface DCGetCache : NSObject

+ (void)getLoadType:(int)loadType;
+ (int)setLoadType;

@end
