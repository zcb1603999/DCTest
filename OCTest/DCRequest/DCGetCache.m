

#import "DCGetCache.h"

@implementation DCGetCache

static int _loadType;

+ (void)getLoadType:(int)loadType{
    
    _loadType = loadType;
}

+ (int)setLoadType{
    
    return _loadType;
}

@end
