/**
 *  请求类
 */


#import <Foundation/Foundation.h>
#import "DCResponse.h"

@interface DCRequest : NSObject

typedef NS_ENUM(NSUInteger, DCRequestType){
    DCRequestTypeGet    = 0,
    DCRequestTypePost   = 1,
    DCRequestTypePut    = 2,
    DCRequestTypeDelete = 3,
};

/**基本的请求链接*/
//@property (nonatomic, copy) NSString *baseURL;

/**
 *  单例模式
 *
 */
+ (instancetype)shareDCRequest;


/**
 *  注册请求
 *
 *  @param baseUrl     请求链接URL
 *  @param path        请求路径
 *  @param requestType 请求类型
 *
 *  @return 返回注册的请求
 */
- (id)initWithBaseURL:(NSString *)baseUrl path:(NSString *)path requestType:(DCRequestType)requestType;

/**
 *  请求参数
 *
 *  @param param 参数值
 *  @param key   参数名
 */
- (void)setParam:(NSObject *)param forKey:(NSString *)key;

/**
 *  获取参数值
 *
 *  @param key 参数名
 *
 *  @return 参数值
 */
- (NSObject *)getParamForKey:(NSString *)key;

/**
 *  根据参数名移除参数
 *
 *  @param key 参数名
 */
- (void)removeParamForKey:(NSString *)key;

/**
 *  移除所有参数
 */
- (void)removeAllParams;

/**
 *  取消请求
 */
- (void)cancelRequest;

/**
 *  请求正常并返回成功结果
 *
 *  @param menuArray   目录参数数组(用到目录参数的调用)
 *  @param callBack 请求结果
 */
- (void)callWithMenu:(nullable NSMutableArray *)menuArray callBack:(nullable void (^)(DCResponse * _Nullable response))callBack;

/**
 *  请求失败,自己init的时候调用
 *
 *  @param menuArray   目录参数数组(用到目录参数的调用)
 *  @param callBack 请求结果
 *  @param error    系统错误提示
 *
 */
- (void)callWithMenu:(nullable NSMutableArray *)menuArray callBack:(nullable void (^)(DCResponse * _Nullable response))callBack error:(nullable void (^)(NSString * _Nullable errorMsg))error;

/**
 *  请求正常并返回成功结果，使用单例的时候调用
 *
 *  @param baseUrl             请求链接URL
 *  @param path                请求路径
 *  @param menuArray           目录参数数组(用到目录参数的调用)
 *  @param requestType         请求类型
 *  @param parameterDictionary 请求参数
 *  @param callBack            请求结果
 */
- (void)callWithWithBaseURL:(nullable NSString *)baseUrl path:(nullable NSString *)path menu:(nullable NSMutableArray *)menuArray requestType:(DCRequestType)requestType parameter:(nullable NSMutableDictionary *)parameterDictionary callBack:(nullable void (^)(DCResponse * _Nullable response))callBack;

/**
 *  请求失败，使用单例的时候调用
 *
 *  @param baseUrl             请求链接URL
 *  @param path                请求路径
 *  @param menuArray           目录参数数组(用到目录参数的调用)
 *  @param requestType         请求类型
 *  @param parameterDictionary 请求参数
 *  @param callBack            请求结果
 *  @param error               系统错误提示
 */
- (void)callWithWithBaseURL:(nullable NSString *)baseUrl path:(nullable NSString *)path menu:(nullable NSMutableArray *)menuArray requestType:(DCRequestType)requestType parameter:(nullable NSMutableDictionary *)parameterDictionary callBack:(nullable void (^)(DCResponse * _Nullable response))callBack error:(nullable void (^)(NSString * _Nullable errorMsg))error;



@end
